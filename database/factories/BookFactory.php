<?php

declare(strict_types=1);

use App\Models\Book;
use App\Models\Genre;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Book::class, function (Faker $faker) {
    $genreId = Genre::query()->inRandomOrder()->value('id');

    return [
        'upc' => Str::random(),
        'title' => $faker->sentence,
        'price' => $faker->randomFloat(null, 1, 100),
        'tax' => $faker->randomFloat(null, 1, 100),
        'cover' => $faker->imageUrl(),
        'rating' => $faker->numberBetween(0, 5),
        'available' => $faker->numberBetween(0, 100),
        'number_of_reviews' => $faker->numberBetween(0, 100),
        'description' => $faker->text,
        'genre_id' => $genreId,
    ];
});
