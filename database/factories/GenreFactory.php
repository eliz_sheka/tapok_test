<?php

declare(strict_types=1);

use App\Models\Genre;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Genre::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
    ];
});
