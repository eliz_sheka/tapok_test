<?php

declare(strict_types=1);

$router->get('books', ['as' => 'book.list','uses' => 'BookController@list']);
