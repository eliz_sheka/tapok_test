<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Genre
 *
 * @package App\Models
 */
class Genre extends Model
{
    /**
     * @var string
     */
    protected $table = 'genres';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'title' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books(): HasMany
    {
        return $this->hasMany(Book::class, 'genre_id', 'id');
    }
}
