<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function basename;
use function file_get_contents;
use function is_string;
use function parse_url;
use function pathinfo;
use function time;

/**
 * Class Book
 *
 * @package App\Models
 */
class Book extends Model
{
    /**
     * @var string
     */
    protected $table = 'books';

    /**
     * @var array
     */
    protected $fillable = [
        'upc',
        'title',
        'price',
        'tax',
        'cover',
        'rating',
        'available',
        'number_of_reviews',
        'description',
        'genre_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'upc' => 'string',
        'title' => 'string',
        'price' => 'float',
        'tax' => 'float',
        'cover' => 'string',
        'rating' => 'int',
        'available' => 'int',
        'number_of_reviews' => 'int',
        'description' => 'string',
        'genre_id' => 'int',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genre(): BelongsTo
    {
        return $this->belongsTo(Genre::class, 'genre_id', 'id', 'genre');
    }

    /**
     * @return float
     */
    public function getFullPriceAttribute(): float
    {
        return $this->getAttribute('tax') + $this->getAttribute('price');
    }

    /**
     * @return string|null
     */
    public function getGenreTitleAttribute(): ?string
    {
        return $this->genre()->value('title');
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setCoverAttribute($value): void
    {
        if ($value && is_string($value)) {
            if ($image = file_get_contents($value)) {
                $urlParts = pathinfo($value);

                $extension = Arr::get($urlParts, 'extension');
                $fileName = Str::random(6).'_'.time().($extension ? ".{$extension}" : '');

                $path = "{$this->getAttribute('upc')}/{$fileName}";

                Storage::put($path, $image);

                $this->attributes['cover'] = $path;
            }

            return;
        }

        $this->attributes['cover'] = null;
    }
}
