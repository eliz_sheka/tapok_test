<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\ParseBooksAction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class Parse
 *
 * @package App\Console\Commands
 */
class Parse extends Command
{
    /**
     * @var string
     */
    protected $signature = 'parse';

    /**
     * @return void
     */
    public function handle(): void
    {
        try {
            (new ParseBooksAction())->execute();
        } catch (Throwable $e) {
            $this->error($e->getMessage());
            Log::channel('errorlog')->info($e->getMessage());
        }
    }
}
