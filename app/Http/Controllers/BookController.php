<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller;

/**
 * Class BookController
 *
 * @package App\Http\Controllers
 */
class BookController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(): JsonResponse
    {
        $books = Book::query()->paginate();

        return new JsonResponse([
            'data' => BookResource::collection($books),
            'meta' => [
                'total' => $books->total(),
                'current_page' => $books->currentPage(),
                'per_page' => $books->perPage(),
                'last_page' => $books->lastPage(),
            ],
        ]);
    }
}
