<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BookResource
 *
 * @package App\Http\Resources
 */
class BookResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'title' => $this->resource->getAttribute('title'),
            'genre' => $this->resource->getAttribute('genre_title'),
            'description' => $this->resource->getAttribute('description'),
            'price' => $this->resource->getAttribute('full_price'),
            'available' => $this->resource->getAttribute('available'),
        ];
    }
}
