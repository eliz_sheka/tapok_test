<?php

declare(strict_types=1);

namespace App\Actions;

use App\Models\Genre;
use Goutte\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;
use Throwable;

use function array_filter, array_merge, array_search, ceil, count, explode,
    ltrim, preg_replace, in_array, trim;

use const false, null, true;

/**
 * Class ParseBooksAction
 *
 * @package App\Actions
 */
class ParseBooksAction
{
    /**
     * @var string
     */
    protected $endpoint = 'http://books.toscrape.com/';

    /**
     * @var \Goutte\Client
     */
    protected $client;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $crawler = $this->client->request('GET', $this->endpoint);

        $li = $crawler->filter('div.side_categories > ul > li')->first();

        $li->filter('ul > li')->each(function ($node) {
            $genreName = $node->text('');

            if (!$genreName) {
                return;
            }

            /** @var \App\Models\Genre $genre */
            $genre = Genre::query()->updateOrCreate([
                'title' => $genreName,
            ]);

            $genrePage = $this->client->click($node->selectLink($genreName)->link());

            $perPage = null;
            $booksRawData = [];
            $totalPerGenre = $genrePage->filter('div > form > strong')->first()->text('');

            $paginationData = $genrePage->filter('div > form > strong');

            $perPage = $paginationData->count() > 1
                ? $paginationData->last()->text('')
                : $totalPerGenre;

            for ($i = 1; $i <= ceil($totalPerGenre/$perPage); $i++) {
                if (1 !== $i) {
                    $nextLink = $genrePage->filter('ul.pager > li.next')->text('');

                    if ($nextLink) {
                        $genrePage = $this->client->click($genrePage->selectLink($nextLink)->link());
                    }
                }

                $booksRawData[] = $this->getPageData($genrePage);
            }

            $booksRawData = array_merge([], ...$booksRawData);

            $numberOfParsed = count($booksRawData);
            $numberOfCreated = 0;

            foreach ($booksRawData as $rawData) {
                $numberOfCreated += $this->create($genre, $rawData);
            }

            if ($totalPerGenre !== $numberOfParsed || $totalPerGenre !== $numberOfCreated) {
                Log::channel('parse')->error(
                    "Genre: {$genre->getKey()} - {$genreName}. Not all data has been processed."
                );
            }

            Log::channel('parse')->info(
                "Genre: {$genre->getKey()} - {$genreName}. Total on the page: {$totalPerGenre}. ".
                "Parsed: {$numberOfParsed}. Created: {$numberOfCreated}"
            );
        });
    }

    /**
     * @param \Symfony\Component\DomCrawler\Crawler $genrePage
     *
     * @return array
     */
    protected function getPageData(Crawler $genrePage): array
    {
        return $genrePage->filter('div > ol.row > li')->each(function ($n) {
            try {
                $rawImagePath = $n->filter('article.product_pod > div.image_container > a > img')->attr('src');
            } catch (Throwable $exception) {
                $rawImagePath = null;
            }

            $bookRawData = $n->filter('article > h3 >a')->each(function ($no) {
                $bookPage = $this->client->click($no->selectLink($no->text())->link());

                $title = $bookPage->filter('div.product_main > h1')->text('');

                try {
                    $rawRating = $bookPage->filter('div.product_main > p.star-rating')->attr('class');
                } catch (Throwable $exception) {
                    $rawRating = null;
                }

                $rawInStock = $bookPage->filter('div.product_main > p.instock.availability')->text('0');
                $description = $bookPage->filter('article.product_page > p')->first()->text('');

                $tableHeaders = [
                    'upc' => 'UPC',
                    'raw_price' => 'Price (excl. tax)',
                    'raw_tax' => 'Tax',
                    'number_of_reviews' => 'Number of reviews',
                ];

                $tableData = $bookPage->filter('table.table.table-striped > tr')
                    ->each(function ($tableRaws) use ($tableHeaders) {
                        $header = $tableRaws->filter('th')->first()->text('');

                        if (false !== ($key = array_search($header, $tableHeaders, true))) {

                            return [$key => $tableRaws->filter('td')->first()->text('')];
                        }

                        return null;
                    });

                $tableData = array_filter($tableData, function ($value) {
                    return $value !== null;
                });

                $tableData = array_merge([], ...$tableData);

                return array_merge(
                    $tableData,
                    [
                        'title' => $title,
                        'raw_in_stock' => $rawInStock,
                        'raw_rating' => $rawRating,
                        'description' => $description,
                    ]
                );
            });

            $bookRawData = array_merge([], ...$bookRawData);
            $bookRawData['raw_image_path'] = $rawImagePath;

            return $bookRawData;
        });
    }

    /**
     * @param \App\Models\Genre $genre
     * @param array $rawData
     *
     * @return int
     */
    protected function create(Genre $genre, array $rawData): int
    {
        $data = array_filter($this->prepareData($rawData), function ($value) {
            return !in_array($value, ['', null], true);
        });

        if ($data) {
            $genre->books()->create($data);

            return 1;
        }

        return 0;
    }

    /**
     * @param array $rawData
     *
     * @return array
     */
    protected function prepareData(array $rawData): array
    {
        $fields = ['upc', 'description', 'title', 'number_of_reviews'];

        foreach ($fields as $field) {
            $data[$field] = ($value = trim(Arr::get($rawData, $field, ''))) === ''
                ? null
                : $value;
        }

        $data['tax'] = $this->parsePrice(Arr::get($rawData, 'raw_tax'));
        $data['price'] = $this->parsePrice(Arr::get($rawData, 'raw_price'));
        $data['rating'] = $this->parseRating(Arr::get($rawData, 'raw_rating'));
        $data['cover'] = $this->parseCover(Arr::get($rawData, 'raw_image_path'));
        $data['available'] = $this->parseAvailable(Arr::get($rawData, 'raw_in_stock'));

        return $data;
    }

    /**
     * @param string|null $price
     *
     * @return float
     */
    protected function parsePrice(string $price = null): ?float
    {
        if (!$price) {
            return null;
        }

        $currency = '£';

        return (float) ltrim($price, $currency);
    }

    /**
     * @param string|null $classes
     *
     * @return int
     */
    protected function parseRating(string $classes = null): ?int
    {
        if (!$classes) {
            return null;
        }

        $ratingMapping = [
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
        ];

        foreach (explode(' ', $classes) as $className) {
            if (false !== ($key = array_search($className, $ratingMapping, true))) {
                return $key;
            }
        }

        return 0;
    }

    /**
     * @param string|null $path
     *
     * @return string|null
     */
    protected function parseCover(string $path = null): ?string
    {
        if (!$path = ltrim($path ?: '', '/.')) {
            return null;
        }

        return "{$this->endpoint}".$path;
    }

    /**
     * @param string|null $available
     *
     * @return int|null
     */
    protected function parseAvailable(string $available = null): ?int
    {
        return $available
            ? (int) preg_replace('/[^0-9]/', '', $available)
            : null;
    }
}
