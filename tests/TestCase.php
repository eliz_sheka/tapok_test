<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function getPaginationJsonSchema(array $data = []): array
    {
        return [
            'data' => [
                '*' => $data,
            ],
            'meta' => [
                'total',
                'current_page',
                'per_page',
                'last_page',
            ],
        ];
    }
}
