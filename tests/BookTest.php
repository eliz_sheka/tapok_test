<?php

use App\Models\Book;
use App\Models\Genre;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\URL;

class BookTest extends TestCase
{
    public function testGenreCreation()
    {
        $number = 10;

        $genres = factory(Genre::class, $number)->create();

        $this->assertCount($number, $genres);
    }

    public function testBookCreation()
    {
        $number = 10;

        $books = factory(Book::class, $number)->create();

        $this->assertCount($number, $books);
    }

    public function testList()
    {
        $number = 10;

        factory(Genre::class, $number)->create();
        factory(Book::class, $number)->create();

        $this->json('GET', URL::route('book.list'))
            ->seeJsonStructure($this->getPaginationJsonSchema([
                'title',
                'genre',
                'description',
                'price',
                'available',
            ]))
            ->assertResponseOk();

        $response = json_decode($this->response->getContent(), true);

        $recordNumber = count(Arr::get($response, 'data'));

        $meta = Arr::get($response, 'meta');
        $total = Arr::get($meta, 'total');
        $perPage = Arr::get($meta, 'per_page');

        $this->assertTrue($number === $total && $recordNumber <= $perPage);
    }
}
